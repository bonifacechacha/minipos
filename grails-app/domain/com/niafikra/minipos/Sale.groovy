package com.niafikra.minipos

class Sale {
    Date date
    String type
    BigDecimal discount
    BigInteger amount

    static belongsTo = [customer:Customer,product:Product]

    static constraints = {
    }
}
