package com.niafikra.minipos

class Category {
    String name

    static hasMany = [products:Product]

    static constraints = {
    }
}
