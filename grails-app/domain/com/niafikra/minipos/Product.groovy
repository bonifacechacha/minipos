package com.niafikra.minipos

class Product {
    String name
    BigDecimal transportCost
    BigDecimal salesCommission
    BigDecimal teamCommision

    BigInteger stock

    static hasMany = [sales:Sale]
    static belongsTo = [category:Category]

    static constraints = {
    }
}
