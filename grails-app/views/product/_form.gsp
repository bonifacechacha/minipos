<%@ page import="com.niafikra.minipos.Product" %>



<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'stock', 'error')} required">
    <label for="stock">
        <g:message code="product.stock.label" default="Stock"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="stock" name="stock.id" from="${com.niafikra.minipos.Stock.list()}" optionKey="id" required=""
              value="${productInstance?.stock?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'category', 'error')} required">
    <label for="category">
        <g:message code="product.category.label" default="Category"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="category" name="category.id" from="${com.niafikra.minipos.Category.list()}" optionKey="id" required=""
              value="${productInstance?.category?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'name', 'error')} ">
    <label for="name">
        <g:message code="product.name.label" default="Name"/>

    </label>
    <g:textField name="name" value="${productInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'sales', 'error')} ">
    <label for="sales">
        <g:message code="product.sales.label" default="Sales"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${productInstance?.sales ?}" var="s">
            <li><g:link controller="sale" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="sale" action="create"
                    params="['product.id': productInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'sale.label', default: 'Sale')])}</g:link>
        </li>
    </ul>

</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'salesCommission', 'error')} required">
    <label for="salesCommission">
        <g:message code="product.salesCommission.label" default="Sales Commission"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="salesCommission" required=""
             value="${fieldValue(bean: productInstance, field: 'salesCommission')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'teamCommision', 'error')} required">
    <label for="teamCommision">
        <g:message code="product.teamCommision.label" default="Team Commision"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="teamCommision" required=""
             value="${fieldValue(bean: productInstance, field: 'teamCommision')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'transportCost', 'error')} required">
    <label for="transportCost">
        <g:message code="product.transportCost.label" default="Transport Cost"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="transportCost" required=""
             value="${fieldValue(bean: productInstance, field: 'transportCost')}"/>
</div>

