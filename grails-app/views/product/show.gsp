<%@ page import="com.niafikra.minipos.Product" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-product" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list product">

        <g:if test="${productInstance?.stock}">
            <li class="fieldcontain">
                <span id="stock-label" class="property-label"><g:message code="product.stock.label"
                                                                         default="Stock"/></span>

                <span class="property-value" aria-labelledby="stock-label"><g:link controller="stock" action="show"
                                                                                   id="${productInstance?.stock?.id}">${productInstance?.stock?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${productInstance?.category}">
            <li class="fieldcontain">
                <span id="category-label" class="property-label"><g:message code="product.category.label"
                                                                            default="Category"/></span>

                <span class="property-value" aria-labelledby="category-label"><g:link controller="category"
                                                                                      action="show"
                                                                                      id="${productInstance?.category?.id}">${productInstance?.category?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${productInstance?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="product.name.label"
                                                                        default="Name"/></span>

                <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${productInstance}"
                                                                                        field="name"/></span>

            </li>
        </g:if>

        <g:if test="${productInstance?.sales}">
            <li class="fieldcontain">
                <span id="sales-label" class="property-label"><g:message code="product.sales.label"
                                                                         default="Sales"/></span>

                <g:each in="${productInstance.sales}" var="s">
                    <span class="property-value" aria-labelledby="sales-label"><g:link controller="sale" action="show"
                                                                                       id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

        <g:if test="${productInstance?.salesCommission}">
            <li class="fieldcontain">
                <span id="salesCommission-label" class="property-label"><g:message code="product.salesCommission.label"
                                                                                   default="Sales Commission"/></span>

                <span class="property-value" aria-labelledby="salesCommission-label"><g:fieldValue
                        bean="${productInstance}" field="salesCommission"/></span>

            </li>
        </g:if>

        <g:if test="${productInstance?.teamCommision}">
            <li class="fieldcontain">
                <span id="teamCommision-label" class="property-label"><g:message code="product.teamCommision.label"
                                                                                 default="Team Commision"/></span>

                <span class="property-value" aria-labelledby="teamCommision-label"><g:fieldValue
                        bean="${productInstance}" field="teamCommision"/></span>

            </li>
        </g:if>

        <g:if test="${productInstance?.transportCost}">
            <li class="fieldcontain">
                <span id="transportCost-label" class="property-label"><g:message code="product.transportCost.label"
                                                                                 default="Transport Cost"/></span>

                <span class="property-value" aria-labelledby="transportCost-label"><g:fieldValue
                        bean="${productInstance}" field="transportCost"/></span>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${productInstance?.id}"/>
            <g:link class="edit" action="edit" id="${productInstance?.id}"><g:message code="default.button.edit.label"
                                                                                      default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
