<%@ page import="com.niafikra.minipos.Customer" %>



<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'address', 'error')} ">
    <label for="address">
        <g:message code="customer.address.label" default="Address"/>

    </label>
    <g:textField name="address" value="${customerInstance?.address}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'mobile', 'error')} ">
    <label for="mobile">
        <g:message code="customer.mobile.label" default="Mobile"/>

    </label>
    <g:textField name="mobile" value="${customerInstance?.mobile}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'name', 'error')} ">
    <label for="name">
        <g:message code="customer.name.label" default="Name"/>

    </label>
    <g:textField name="name" value="${customerInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'sales', 'error')} ">
    <label for="sales">
        <g:message code="customer.sales.label" default="Sales"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${customerInstance?.sales ?}" var="s">
            <li><g:link controller="sale" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="sale" action="create"
                    params="['customer.id': customerInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'sale.label', default: 'Sale')])}</g:link>
        </li>
    </ul>

</div>

