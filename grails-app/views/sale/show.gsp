<%@ page import="com.niafikra.minipos.Sale" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'sale.label', default: 'Sale')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-sale" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                           default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-sale" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list sale">

        <g:if test="${saleInstance?.customer}">
            <li class="fieldcontain">
                <span id="customer-label" class="property-label"><g:message code="sale.customer.label"
                                                                            default="Customer"/></span>

                <span class="property-value" aria-labelledby="customer-label"><g:link controller="customer"
                                                                                      action="show"
                                                                                      id="${saleInstance?.customer?.id}">${saleInstance?.customer?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${saleInstance?.date}">
            <li class="fieldcontain">
                <span id="date-label" class="property-label"><g:message code="sale.date.label" default="Date"/></span>

                <span class="property-value" aria-labelledby="date-label"><g:formatDate
                        date="${saleInstance?.date}"/></span>

            </li>
        </g:if>

        <g:if test="${saleInstance?.discount}">
            <li class="fieldcontain">
                <span id="discount-label" class="property-label"><g:message code="sale.discount.label"
                                                                            default="Discount"/></span>

                <span class="property-value" aria-labelledby="discount-label"><g:fieldValue bean="${saleInstance}"
                                                                                            field="discount"/></span>

            </li>
        </g:if>

        <g:if test="${saleInstance?.product}">
            <li class="fieldcontain">
                <span id="product-label" class="property-label"><g:message code="sale.product.label"
                                                                           default="Product"/></span>

                <span class="property-value" aria-labelledby="product-label"><g:link controller="product" action="show"
                                                                                     id="${saleInstance?.product?.id}">${saleInstance?.product?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${saleInstance?.type}">
            <li class="fieldcontain">
                <span id="type-label" class="property-label"><g:message code="sale.type.label" default="Type"/></span>

                <span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${saleInstance}"
                                                                                        field="type"/></span>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${saleInstance?.id}"/>
            <g:link class="edit" action="edit" id="${saleInstance?.id}"><g:message code="default.button.edit.label"
                                                                                   default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
