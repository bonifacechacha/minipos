<%@ page import="com.niafikra.minipos.Sale" %>



<div class="fieldcontain ${hasErrors(bean: saleInstance, field: 'customer', 'error')} required">
    <label for="customer">
        <g:message code="sale.customer.label" default="Customer"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="customer" name="customer.id" from="${com.niafikra.minipos.Customer.list()}" optionKey="id" required=""
              value="${saleInstance?.customer?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleInstance, field: 'date', 'error')} required">
    <label for="date">
        <g:message code="sale.date.label" default="Date"/>
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="date" precision="day" value="${saleInstance?.date}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleInstance, field: 'discount', 'error')} required">
    <label for="discount">
        <g:message code="sale.discount.label" default="Discount"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="number" name="discount" required="" value="${fieldValue(bean: saleInstance, field: 'discount')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleInstance, field: 'product', 'error')} required">
    <label for="product">
        <g:message code="sale.product.label" default="Product"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="product" name="product.id" from="${com.niafikra.minipos.Product.list()}" optionKey="id" required=""
              value="${saleInstance?.product?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleInstance, field: 'type', 'error')} ">
    <label for="type">
        <g:message code="sale.type.label" default="Type"/>

    </label>
    <g:textField name="type" value="${saleInstance?.type}"/>
</div>

